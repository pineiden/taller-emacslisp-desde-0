#+TITLE: Algunos algoritmos de precalentamiento

* Números Primos
:properties:
:header-args:python: :results output :session primos
:header-args:lisp: :results output :session primos
:end:

Dede un lenguaje popular, como python, encontrar los primeros números
primos puede ser una tarea sencilla. 

Se puede hacer por iteración o recursión, en los cuales los principal
es la posibilidad de usar el operador módulo *%* que entrega 0 si la
división es entera.

Para un número múltiplo del divisor se tiene.

#+BEGIN_SRC python
print(10%5)
#+END_SRC

#+RESULTS:
: 0

Para un número no múltiplo de divisor.

#+name: con_resto
#+BEGIN_SRC python
print(9%2)
#+END_SRC

La división no es entera, sobra '1'.

#+RESULTS: con_resto
: 1

Por definición un *número primo* lo es cuando es divisible por 1 y si
mismo, por lo tanto la división por cualquier otro número será
diferente a 0. Como propiedad, todo número es divisible por 1, por lo
que nos interesan los casos superiores a 1.

De 1 a 100, ¿cuáles serían primos?

#+BEGIN_SRC python
primos = {1}
for i in range(2,10):
  divisores = []
  for j in primos:
    if i%j == 0:
        divisores.append(j)
  if len(divisores) == 1:
    primos.add(i)
print("Primos", primos)
#+END_SRC

#+RESULTS:
: ('Primos', set([1, 2, 3, 5, 7]))

Este código, en una función, podría entregarse de la siguiente manera.

#+BEGIN_SRC python 
def primos(n):
    primos = {1}
    for i in range(2,n):
      divisores = []
      for j in primos:
	if i%j == 0:
	    divisores.append(j)
      if len(divisores) == 1:
	primos.add(i)
    return primos
#+END_SRC

#+RESULTS:

Y se puede utilizar llamando la función.

#+BEGIN_SRC python 
from pprint import pprint
ps = primos(100)
pprint(ps)
#+END_SRC

#+RESULTS:
#+begin_example
set([1,
     2,
     3,
     5,
     7,
     11,
     13,
     17,
     19,
     23,
     29,
     31,
     37,
     41,
     43,
     47,
     53,
     59,
     61,
     67,
     71,
     73,
     79,
     83,
     89,
     97])
#+end_example

** ¿Cómo se haría en lisp?

En *lisp* deberemos buscar

- operador módulo
- iteración
- condicional
- agregar elemento a lista

Existe la función *mod* que realiza la misma operación que *%* en
python.

#+BEGIN_SRC lisp
(mod 10 5)
#+END_SRC

#+RESULTS:
: 0

En *emacs* también existe

#+BEGIN_SRC emacs-lisp
(mod 10 5)
#+END_SRC

#+RESULTS:
: 0

Utilizando variables

#+BEGIN_SRC lisp
(setq a 5)
(print (mod a 2))
(print (mod a 3))
(print (mod a 4))
#+END_SRC

#+RESULTS:
: 
: 1 
: 2 
: 1 

Viendo iteraciones de la documentación
https://lispcookbook.github.io/cl-cookbook/iteration.html#dotimes

Para iterar en un rango, basca con escribir:

Crear lista de primos inicial

#+BEGIN_SRC lisp
(setq primos (cons '1 '()))
(print primos)
#+END_SRC

#+RESULTS:
: 
: (1) 

Necesitaremos concatenar la lista de primos a nuevos valores.

#+BEGIN_SRC lisp
(setq primos (cons '3 '(2 1)))
(print primos)
(print (cdr primos))
#+END_SRC

#+RESULTS:
: 
: (3 2 1) 
: (2 1) 

Y directamente concatenate

#+BEGIN_SRC lisp
(print (concatenate 'list primos '(5 6 7)))
#+END_SRC

#+RESULTS:
: 
: (3 2 1 5 6 7) 


#+BEGIN_SRC lisp
(setq a 10)
(print (concatenate 'list primos (list a)))
(setq primos (concatenate 'list primos (list a)))
(print primos)

#+END_SRC

#+RESULTS:
: 
: (3 2 1 10) 
: (3 2 1 10) 

O, con append

#+BEGIN_SRC lisp
(print (append (list 1 2) (list 4 6)))
#+END_SRC

#+RESULTS:
: 
: (1 2 4 6) 


Iteracíón y concatenación

#+BEGIN_SRC lisp
(setq primos (cons '1 '()))
(loop for a from 2 to 10
   do (
      loop for b from 2 to 5
      when (<= a b)
      do (
        setq primos (concatenate 'list primos (list b))
	)
    )
)
(print primos)
#+END_SRC

#+RESULTS:
: 
: (1 2 3 4 5 3 4 5 4 5 5) 


Iteración

#+BEGIN_SRC lisp
  (setq primos (cons '1 '()))
  (print primos)
  (loop for a from 2 to 100
	do (setq divisores '())
	do(
	loop for b in (cdr primos)
	when (eq (mod a b) 0)
	do (
	  setq divisores (concatenate 'list divisores (list b))
	  )
        )
	when (eq (length divisores) 0)
	do (
	  setq primos (concatenate 'list primos (list a))         
	 )
  )
  (print primos)
#+END_SRC

#+RESULTS:
: 
: (1) 
: (1 2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97) 


Ahora, sabien que el código opera correctamente para diferentes casos,
creamos la función.


#+BEGIN_SRC lisp
(defun listar_primos (n)
  "Entrega lista de números primos, desde 1 hasta n"
  (setq primos (cons '1 '()))
  (loop for a from 2 to n
	do (setq divisores '())
	do(
	loop for b in (cdr primos)
	when (eq (mod a b) 0)
	do (
	  setq divisores (concatenate 'list divisores (list b))
	  )
        )
	when (eq (length divisores) 0)
	do (
	  setq primos (concatenate 'list primos (list a))         
	 )
  )
  primos
)
#+END_SRC

#+RESULTS:


#+BEGIN_SRC lisp
(print (listar_primos 50))
#+END_SRC

#+RESULTS:
: 
: (1 2 3 5 7 11 13 17 19 23 29 31 37 41 43 47) 

* Ordenar valores

#+BEGIN_SRC lisp
(defun comparar (a b)
(< a b)
)
(sort '(1 3 4 2 10 7) 'comparar)
#+END_SRC

#+RESULTS:
| 1 | 2 | 3 | 4 | 7 | 10 |


