import os
from pprint import pprint

print("Esta es una prueba de python")


array_diccionario = [
    {"nombre":"José", "mascota":"Perico"},
    {"nombre":"María", "mascota":"Lazca"},
    {"nombre":"Daniela", "mascota":"Nini"},
    {"nombre":"Martín", "mascota":"Pokilo"},
    {"nombre":"Reinaldo", "mascota":"Quox"}
]


[pprint(elem, indent=4) for elem in array_diccionario]
