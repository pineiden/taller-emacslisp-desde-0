;; activamos paquetería:

(require 'package)
(add-to-list 'package-archives  '("melpa-stable" . "https://stable.melpa.org/packages/")) 
(package-initialize)


(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure 't)


;;personalizamos algunas características visuales de emacs

(setq inhibit-startup-message t)
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)

;; Theme
(use-package dracula-theme
	     :init (load-theme 'dracula t))

;; definimos el buffer *scratch* donde evaluar código lisp

(setq initial-scratch-message ";; Scratch Buffer \n\n"
      initial-major-mode 'org-mode)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages (quote (olivetti dracula-theme use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; salto de linea sensible
(add-hook 'text-mode-hook 'visual-line-mode)

;; org-mode basico
(use-package org
  :config
  (global-set-key (kbd "C-c l") 'org-store-link)
  (global-set-key (kbd "C-c a") 'org-agenda)
  (global-set-key (kbd "C-c c") 'org-capture)
  ;; org mode ricing
  (setq org-hide-leading-stars t
	org-startup-indented t
	org-hide-emphasis-markers t
	org-image-actual-width 600
	org-startup-with-inline-images t))

(setq-default org-src-tab-acts-natively t)
(setq-default org-src-preserve-indentation t)
(setq-default org-edit-src-content-indentation 2)
(setq-default org-adapt-indentation nil)
(setq-default electric-indent-mode nil)
(setq default-tab-width 4)

;; enable org-babel

(require 'ob-gnuplot)

(setq org-image-actual-width 600)

(message "Loading org-mode babel variables")

(org-babel-do-load-languages
 'org-babel-load-languages
 (append org-babel-load-languages
 '(
   (awk . t)
   (calc .t)
   (C . t)
   (emacs-lisp . t)
   (haskell . t)
   (gnuplot . t)
   (latex . t)
   (js . t)
   (haskell . t)
   (perl . t)
   (python . t)
   (gnuplot . t)
   (plantuml .t)
   (org . t)
   (R . t)
   (scheme . t)
   (sql . t)
   (octave . t)
   (dot . t)
   (ditaa .t)
   (shell .t)
   ;;(sqlite . t)
   ;;(kotlin . t)
   )))

(message "confirm languages auto -run ")
;;; don't ask to run in org-mode:
(defun my-org-confirm-babel-evaluate (lang body) }
       (not (member lang '(
                           "bash"
                           "shell"
                           "python"
                           "awk"
                           "gnuplot"
                           "haskell"
                           "sed"
                           "javascript"
                           "go"
                           "rust"
                           "c"
                           "c++"
                           "sql"
                           "octave"
                           "dot"
                           "ditaa"
                           ))))
(setq org-babel-check-confirm-evaluate 'my-org-confirm-babel-evaluate)
(setq org-link-shell-confirm-function nil)
(setq org-link-elisp-confirm-function nil)

;; sobreescribir texto seleiconado
(delete-selection-mode t)

;; revisión de ortografia

;; (use-package flyspell
;;   :config
;;   (add-hook 'text-mode-hook 'flyspell-mode)
;;   (setq ispell-program-name "hunspell"))


;;sin distracciones

;; (use-package olivetti
;;   :config
;;   (defun distraction-free()
;;     "Ambiente sin distracciones"
;;     (interactive)
;;     (if (equal ilivetti-mode nil)
;; 	(progn
;; 	  (delete-other-windows)
;; 	  (text-scale-increase 2)
;; 	  (setq olivetti-body-width 100)
;; 	  (olivetty-mode t))
;;       (progn
;; 	(olivetti-mode 0)
;; 	(text-scale-decrease 2))))

;;   (global-set-key (kbd "<f9>") 'distraction-free)
;;   )



;; start full screen

(add-hook 'emacs-startup-hook 'toggle-frame-maximized)


(global-set-key (kbd "C-+") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)
