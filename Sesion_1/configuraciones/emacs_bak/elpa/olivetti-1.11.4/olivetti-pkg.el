;;; -*- no-byte-compile: t -*-
(define-package "olivetti" "1.11.4" "Minor mode for a nice writing environment" '((emacs "24.4")) :commit "6902410cd857385a3c1aa20ba391901a78d2740b" :authors '(("Paul W. Rankin" . "pwr@bydasein.com")) :maintainer '("Paul W. Rankin" . "pwr@bydasein.com") :keywords '("wp" "text") :url "https://github.com/rnkn/olivetti")
