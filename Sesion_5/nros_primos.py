def nros_primos(n):
    primos = [1]
    for i in range(2,n):
        bandera = True
        for j in primos:
            if i%j == 0 and j > 1:
                bandera = False
                break
        if bandera:
            primos.append(i)
    return primos


print(nros_primos(19))
