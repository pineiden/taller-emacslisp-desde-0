(defun nros_primos (n)
  "Entrega lista de números primos, desde 1 hasta n"
  (setq primos (cons '1 '()))
  (loop for a from 2 to n
	do (setq bandera t)
	do(
	loop for b in (cdr primos)
	when (and (eq (mod a b) 0) (> b 1))
	do (
	  setq bandera nil)
	do (break) 
	  )
        )
	when (eq bandera t)
	do (
	  setq primos (concatenate 'list primos (list a))         
	 )
  )
  primos
)
