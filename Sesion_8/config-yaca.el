;; look
(menu-bar-mode 0)
(tool-bar-mode 0)
(set-scroll-bar-mode nil)
(setq inhibit-startup-screen t)
(display-time-mode)
(global-set-key (kbd "<C-wheel-up>") 'text-scale-increase)
(global-set-key (kbd "<C-wheel-down>") 'text-scale-decrease)

;; Init time on modeline, by systemcrafters.
;; The default is 800 kilobytes.  Measured in bytes.
(setq gc-cons-threshold (* 50 1000 1000))
(defun efs/display-startup-time ()
  (message "Emacs loaded in %s with %d garbage collections."
           (format "%.2f seconds"
                   (float-time
                    (time-subtract after-init-time before-init-time)))
           gcs-done))
(add-hook 'emacs-startup-hook #'efs/display-startup-time)

;; Backups on one directory and not over the whole system
(defvar respaldos "~/.emacs.d/respaldos")
(if (not (file-exists-p respaldos))
    (make-directory respaldos))
(add-to-list 'backup-directory-alist (cons ".*" respaldos))

;; Packages
(require 'package)
(if (version< emacs-version "28")
    (add-to-list 'package-archives '("nongnu" . "https://elpa.nongnu.org/nongnu/") t))
(package-initialize)
(if (not (file-exists-p "~/.emacs.d/elpa"))
    (package-refresh-contents))
(setq package-selected-packages '(pinentry
				  company
				  magit
				  org-contrib
				  slime
				  htmlize))
(if (version< emacs-version "28")
    (add-to-list 'package-selected-packages 'modus-themes))
(package-install-selected-packages)

;; Company
(add-hook 'after-init-hook 'global-company-mode)
(with-eval-after-load 'company
  (setq company-minimum-prefix-length 1
	company-idle-delay 0.0))

;; Theme
(cond ((version< emacs-version "28")
       (add-hook 'after-init-hook 'modus-themes-load-themes)
       (modus-themes-load-vivendi))
      (t (load-theme 'modus-vivendi)))
(global-set-key (kbd "<f7>") 'modus-themes-toggle)

;; Common Lisp
(with-eval-after-load 'slime
  (setq inferior-lisp-program "clisp"))

;; Orgmode
(eval-after-load ".org"
  '((require 'ox-md) ; Markdown on org-export-dispatch
    (setq org-src-preserve-indentation nil   ;
	  org-src-tab-acts-natively t        ; Org mode indentation
	  org-edit-src-content-indentation 0;
	  org-ellipsis "⤵")
    (require 'ox-publish)
    (require 'ox-rss); Blog publishing.
    (defun me/org-sitemap-format-entry (entry style project)
      "Format posts with author and published data in the index page.
ENTRY: file-name
STYLE:
PROJECT: `posts in this case."
      (cond ((not (directory-name-p entry))
	     (format "*[[file:%s][%s]]*
		   #+HTML: <p class='pubdate'>por %s en  %s.</p>"
		     entry
		     (org-publish-find-title entry project)
		     (car (org-publish-find-property entry :author project))
		     (format-time-string this-date-format
					 (org-publish-find-date entry project))))
	    ((eq style 'tree) (file-name-nondirectory (directory-file-name entry)))
	    (t entry)))
    (defvar this-date-format "%b %d, %Y")
    (setq org-publish-project-alist
	  '(
	    ("fuente"
	     :base-directory "~/drimyswinteri/org/"
	     :base-extension "org"
	     ;;:publishing-directory "~/drimyswinteri/public_html/"
	     :publishing-directory "/ssh:drimyswinteri.ml:/home/yaca/drimyswinteri.ml/public_html/"
	     :recursive t
	     :publishing-function org-html-publish-to-html
	     :headline-levels 4
	     :html-preamble t
	     :auto-sitemap t
	     :sitemap-filename "posts.org"
	     :sitemap-title ""
	     :sitemap-format-entry me/org-sitemap-format-entry
	     :sitemap-style list
	     :sitemap-sort-files anti-chronologically
	     :html-head "<link rel=\"stylesheet\"
		    href=\"static/css/drimyswinteri.css\" type=\"text/css\"/>")
	    ("static"
	     :base-directory "~/drimyswinteri/org/"
	     :base-extension "css\\|png\\|ttf\\|gif\\|jpg"
	     :publishing-directory "~/drimyswinteri/public_html/"
	     ;;:publishing-directory "/ssh:drimyswinteri.ml:/home/yaca/drimyswinteri.ml/public_html/"
	     :recursive t
	     :publishing-function org-publish-attachment)
	    ("blog-rss"
	     :base-directory "~/drimyswinteri/org/"
	     :base-extension "org"
	     :publishing-directory "~/drimyswinteri/public_html/"
	     :publishing-function org-rss-publish-to-rss
	     :html-link-home "https://drimyswinteri.ml"
	     :html-link-use-abs-url t
	     :title "Drimyswinteri"
	     :rss-image-url "https://thibaultmarin.github.io/blog/images/feed-icon-28x28.png"
	     :section-number nil
	     :exclude ".*"
	     :include ("posts.org")
	     :table-of-contents nil)
	    ("blog"
	     :components ("fuente" "static" "blog-rss"))))))

;; Dired
(with-eval-after-load 'dired
  (require 'dired-x)
  (add-hook 'dired-mode-hook 'toggle-truncate-lines) ; Better view on dired
  (setq dired-guess-shell-alist-user '(("\\.mp3\\'\\|\\.flac\\'" "mpv")
				       ("\\.mp4\\'\\|\\.mkv\\'" "mpv")
				       ("\\.jpe?g\\'" "imv")
				       ("\\.pptx?\\'\\|\\.wordx?\\'\\|\\.xlsx?\\'" "libreoffice"))
	dired-listing-switches "-lh") ; show file size in dired
  (defun yaca/dired-open ()
    "Inicia un comando de manera asincrónica si es que está
   definido previamente en 'dired-guess-shell-alist-user o lo abre
   en emacs en caso contrario"
    (interactive)
    (let*
	(
	 ;; El nombre del archivo debe asignarse a una lista para poder
	 ;; ser aceptado como parámetro en otras funciones.
	 (archivo (list (dired-get-filename)))
	 ;; Se obtiene el comando definido previamente y se asigna a la
	 ;; variable 'comando-definido.
	 (comando-definido (dired-guess-default archivo)))
      (if (null comando-definido)
	  ;; Si no existe un comando definido anteriormente entonces se
	  ;; abre en emacs.
	  (dired-find-file)
	;; Si existe el comando, lo corre de manera asíncrona.
	(dired-run-shell-command
	 (dired-shell-stuff-it (concat comando-definido " &") archivo nil)))))
  (define-key dired-mode-map (kbd "<return>") #'yaca/dired-open)
  (add-to-list 'display-buffer-alist
	       (cons "*Async Shell Command*.*" (cons #'display-buffer-no-window nil)))
  (setq async-shell-command-buffer 'new-buffer))

;; Security
;;
;; By default emacs uses Netrc (.authinfo) file to store sensitive
;; data (like the sudo password when using tramp). This type of file
;; is unencrypted and so, no reliable. With the following code we can
;; tell emacs to use .authinfo.gpg which will be a symetrical
;; encrypted file storing our passwords (don't share the file with
;; anyone). In order to pinentry.el to work you should add
;; "allow-emacs-pinentry" to ~/.gnupg/gpg-agent.conf
(setq auth-sources '("~/.authinfo.gpg"))
(pinentry-start)

;; IRC
(with-eval-after-load 'erc ; Lazy load to optimize init time
  (setq erc-nick "DrimysWinteri"
	erc-server "irc.libera.chat"
	erc-port 6667
	erc-prompt-for-password nil))

;; Tramp
;;
;; replace de file to visit with:
;; /method:user@host:folder
(global-set-key [f5] (lambda () (interactive) (find-file "/ssh:yaca@drimyswinteri.ml:~")))

;; Magit
(global-set-key [f8] 'magit-status)
