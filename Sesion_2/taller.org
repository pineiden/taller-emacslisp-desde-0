#+TITLE: Taller de emacs s02


* Configuración desde un Orgmode
Guía de configuración [[https://ryan.himmelwright.net/post/org-babel-setup/][enlace]]

Probamos la configuración.

#+BEGIN_SRC python :results output
print("Configurado desde config.org")
#+END_SRC

#+RESULTS:
: Configurado desde config.org

* Selección de texto de estudio


Opciones

| Nombre                              | Páginas | Prioridad |
|-------------------------------------+---------+-----------|
| Curso de programación de Lisp       |     112 |           |
| GNU Emacs Lisp reference Manual     |    1337 |           |
| Introduction to programming in Lisp |     314 |           |
| Learning GNU Emacs                  |         |           |
| Manual de Lisp                      |      50 |           |
| SICP                                |     883 |           |


#+BEGIN_SRC lisp :output results
(+ 3 4)
#+END_SRC

#+RESULTS:
: 7


* Los 7 operadores primitivos

Define una *expresióń*. Es también un  *átomo*. Que es una secuencia de letras.
O también puede ser una lista de cero o más expresiones. Separado por
espacio, encerrado en paréntesis  

#+BEGIN_EXAMPLE
buu
()
(buu)
(buu bua)
(a b (c) d)
#+END_EXAMPLE

** Quote

Vamos probando, un elemento en 'quote' o string.

#+BEGIN_SRC emacs-lisp
(quote a)
#+END_SRC

#+RESULTS:
: a

#+BEGIN_SRC lisp
(quote a)
#+END_SRC

#+RESULTS:
: A


** Atom

El *atom* de un elemento retorna:

- t o T :: su el valor no es vacio
- NIL o () :: si el valor es vacio

#+BEGIN_SRC lisp
(atom 'a)
#+END_SRC

#+RESULTS:
: T

Que pasa si hacemos atom de T

#+BEGIN_SRC lisp
(atom T)
#+END_SRC

#+RESULTS:
: T

Veamos que retorna sobre una lista de elementos.

#+BEGIN_SRC lisp
(atom '(a b c))
#+END_SRC

#+RESULTS:
: NIL

En emacs, tendríamos 'nada' como resultado.

#+BEGIN_SRC emacs-lisp :results output
(atom '(a b c))
#+END_SRC

#+RESULTS:

Probando otras combinacion.

En este caso el /atom/ interno pasa a ser un elemento de una lista.

#+BEGIN_SRC lisp
(atom '(atom 'a))
#+END_SRC

#+RESULTS:
: NIL

En este caso, /atom/ interno es una función que se ejecuta.

#+BEGIN_SRC lisp
(atom (atom 'a))
#+END_SRC

#+RESULTS:
: T


** Comparación con eq


La primitiva *eq* compara dos elemementos, retorna un *boolean*

- t :: si son iguales
- () :: si no son iguales


#+BEGIN_SRC lisp
(eq 100 100)
#+END_SRC

#+RESULTS:
: T

El átom "'100" lo toma como un elemento numerico. Todo lo interpreta
como símbolo  (si no es función). El comparador *eq* compara por símbolos.

#+BEGIN_SRC lisp
(eq 100 '100)
#+END_SRC

#+RESULTS:
: T


#+BEGIN_SRC lisp
(eq 'a 'a)
#+END_SRC

#+RESULTS:
: T


#+BEGIN_SRC lisp
(eq 'a 'b)
#+END_SRC

#+RESULTS:
: NIL


** Operador *car* 

El operador  *car* rescata el primer elemento de una lista.

#+BEGIN_SRC lisp
(car '(a b c))
#+END_SRC

#+RESULTS:
: A

** Operador *cdr* 

Retorna todos los elementos de una lista, descartando el primero.

#+BEGIN_SRC lisp :results value
(cdr '(1 2 3 4 5))
#+END_SRC

#+RESULTS:
| 2 | 3 | 4 | 5 |

** Operador *cons* 

#+BEGIN_EXAMPLE
(cons x y)
#+END_EXAMPLE
Este operador espera una lista para *y*, retorna una lista contiendo el valor
de *x* y los de *y*.

#+BEGIN_SRC lisp
(cons 'a '(b c))
#+END_SRC

#+RESULTS:
| A | B | C |

Hacemos una conjugación de elementos partiendo de ().

#+BEGIN_SRC lisp
(cons 'a (cons 'b (cons 'c ())))
#+END_SRC

#+RESULTS:
| A | B | C |

Combinando con  *car* o *cdr*

#+BEGIN_SRC lisp
(car (cons 'a '(b c)))
#+END_SRC

#+RESULTS:
: A

Obtenemos la cola

#+BEGIN_SRC lisp
(cdr (cons 'a '(b c)))
#+END_SRC

#+RESULTS:
| B | C |


** Operador *cond*

#+BEGIN_EXAMPLE
(cond (p1 e1)....(pn en))
#+END_EXAMPLE


Las expresiones de *p* son evaluadas en orden hasta que alguna retorne
en *T*. Cuando una coincidencia ocurre, el valor de *e*
correspondiente retorna.

#+BEGIN_SRC lisp
(cond ((eq 'a 'b) 'first) ((atom 'a) 'second))
#+END_SRC

#+RESULTS:
: SECOND

** Crear funciones.

Para definir funciones podemos utilizar el operador *lambda*. Se
expresa como.

#+BEGIN_EXAMPLE
((lambda (p1 .... pn) e) a1....an)
#+END_EXAMPLE

Se llama *función llamada* (función anónima). 

- se evalua cada *ai*
- entonces *e* se evalue
- durante la evaluación de *e*
- el valor de cualquier ocurrencia de uno de los *pi* es el valor
  correspondiente de *ai*

#+BEGIN_SRC lisp
((lambda 
  (x) 
  (cons x '(b))
 ) 
 'a
)
#+END_SRC

#+RESULTS:
| A | B |


#+BEGIN_SRC emacs-lisp
((lambda 
  (x) 
  (cons x '(b))
 ) 
 'a
)
#+END_SRC

#+RESULTS:
| a | b |


Otro ejemplo de *lambda*

#+BEGIN_SRC lisp
((lambda
(x y)
(cons x (cdr y)))
'z
'(a b c)
)
#+END_SRC

#+RESULTS:
| Z | B | C |

Si una expresion tiene como primer elemento un *atom* 
Se puede intercambiar lambdas con funciones ya definidas, ya que en la
practica operan o permiten obtener lo mismo.


[NO ESTA IMPLEMENTADO EN COMMON NI EMACS]
#+BEGIN_SRC emacs-lisp 
((lambda (f) (f '(b c)))
'(lambda (x) (cons 'a x))
)
#+END_SRC
